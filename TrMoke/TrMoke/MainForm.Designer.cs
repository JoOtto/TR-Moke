﻿namespace WindowsFormsApp1
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            OxyPlot.PlotModel plotModel1 = new OxyPlot.PlotModel();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.plot1 = new OxyPlot.WindowsForms.Plot();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gp_Plot = new System.Windows.Forms.GroupBox();
            this.gp_Status = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.gp_Plot.SuspendLayout();
            this.gp_Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // plot1
            // 
            this.plot1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plot1.KeyboardPanHorizontalStep = 0.1D;
            this.plot1.KeyboardPanVerticalStep = 0.1D;
            this.plot1.Location = new System.Drawing.Point(3, 16);
            plotModel1.AnnotationFont = null;
            plotModel1.AnnotationFontSize = 12D;
            plotModel1.Annotations = ((System.Collections.ObjectModel.Collection<OxyPlot.Annotation>)(resources.GetObject("plotModel1.Annotations")));
            plotModel1.AutoAdjustPlotMargins = true;
            plotModel1.Axes = ((System.Collections.ObjectModel.Collection<OxyPlot.Axis>)(resources.GetObject("plotModel1.Axes")));
            plotModel1.AxisTierDistance = 4D;
            plotModel1.Background = null;
            plotModel1.Culture = null;
            plotModel1.DefaultColors = ((System.Collections.Generic.IList<OxyPlot.OxyColor>)(resources.GetObject("plotModel1.DefaultColors")));
            plotModel1.IsLegendVisible = true;
            plotModel1.LegendBackground = null;
            plotModel1.LegendBorder = null;
            plotModel1.LegendBorderThickness = 1D;
            plotModel1.LegendColumnSpacing = 0D;
            plotModel1.LegendFont = null;
            plotModel1.LegendFontSize = 12D;
            plotModel1.LegendFontWeight = 400D;
            plotModel1.LegendItemAlignment = OxyPlot.HorizontalTextAlign.Left;
            plotModel1.LegendItemOrder = OxyPlot.LegendItemOrder.Normal;
            plotModel1.LegendItemSpacing = 24D;
            plotModel1.LegendMargin = 8D;
            plotModel1.LegendOrientation = OxyPlot.LegendOrientation.Vertical;
            plotModel1.LegendPadding = 8D;
            plotModel1.LegendPlacement = OxyPlot.LegendPlacement.Inside;
            plotModel1.LegendPosition = OxyPlot.LegendPosition.RightTop;
            plotModel1.LegendSymbolLength = 16D;
            plotModel1.LegendSymbolMargin = 4D;
            plotModel1.LegendSymbolPlacement = OxyPlot.LegendSymbolPlacement.Left;
            plotModel1.LegendTitle = null;
            plotModel1.LegendTitleFont = null;
            plotModel1.LegendTitleFontSize = 12D;
            plotModel1.LegendTitleFontWeight = 700D;
            plotModel1.PlotAreaBackground = null;
            plotModel1.PlotAreaBorderColor = ((OxyPlot.OxyColor)(resources.GetObject("plotModel1.PlotAreaBorderColor")));
            plotModel1.PlotAreaBorderThickness = 1D;
            plotModel1.PlotControl = this.plot1;
            plotModel1.PlotType = OxyPlot.PlotType.XY;
            plotModel1.Series = ((System.Collections.ObjectModel.Collection<OxyPlot.Series>)(resources.GetObject("plotModel1.Series")));
            plotModel1.Subtitle = null;
            plotModel1.SubtitleFont = null;
            plotModel1.SubtitleFontSize = 14D;
            plotModel1.SubtitleFontWeight = 400D;
            plotModel1.TextColor = ((OxyPlot.OxyColor)(resources.GetObject("plotModel1.TextColor")));
            plotModel1.Title = null;
            plotModel1.TitleFont = null;
            plotModel1.TitleFontSize = 18D;
            plotModel1.TitleFontWeight = 700D;
            plotModel1.TitlePadding = 6D;
            this.plot1.Model = plotModel1;
            this.plot1.Name = "plot1";
            this.plot1.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plot1.Size = new System.Drawing.Size(955, 469);
            this.plot1.TabIndex = 0;
            this.plot1.Text = "plot1";
            this.plot1.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plot1.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plot1.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1864, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mainToolStripMenuItem
            // 
            this.mainToolStripMenuItem.Name = "mainToolStripMenuItem";
            this.mainToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.mainToolStripMenuItem.Text = "Main";
            // 
            // gp_Plot
            // 
            this.gp_Plot.Controls.Add(this.plot1);
            this.gp_Plot.Location = new System.Drawing.Point(891, 141);
            this.gp_Plot.Name = "gp_Plot";
            this.gp_Plot.Size = new System.Drawing.Size(961, 488);
            this.gp_Plot.TabIndex = 2;
            this.gp_Plot.TabStop = false;
            this.gp_Plot.Text = "Plot";
            // 
            // gp_Status
            // 
            this.gp_Status.Controls.Add(this.label17);
            this.gp_Status.Controls.Add(this.label19);
            this.gp_Status.Controls.Add(this.label16);
            this.gp_Status.Controls.Add(this.label12);
            this.gp_Status.Controls.Add(this.label13);
            this.gp_Status.Controls.Add(this.label14);
            this.gp_Status.Controls.Add(this.label15);
            this.gp_Status.Controls.Add(this.label11);
            this.gp_Status.Controls.Add(this.label10);
            this.gp_Status.Controls.Add(this.label9);
            this.gp_Status.Controls.Add(this.label8);
            this.gp_Status.Controls.Add(this.label7);
            this.gp_Status.Controls.Add(this.label6);
            this.gp_Status.Controls.Add(this.label5);
            this.gp_Status.Controls.Add(this.label4);
            this.gp_Status.Controls.Add(this.label3);
            this.gp_Status.Controls.Add(this.label2);
            this.gp_Status.Controls.Add(this.label1);
            this.gp_Status.Location = new System.Drawing.Point(12, 635);
            this.gp_Status.Name = "gp_Status";
            this.gp_Status.Size = new System.Drawing.Size(1840, 182);
            this.gp_Status.TabIndex = 3;
            this.gp_Status.TabStop = false;
            this.gp_Status.Text = "Status";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Red;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(6, 119);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(319, 55);
            this.label17.TabIndex = 19;
            this.label17.Text = "measurement";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(279, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(242, 39);
            this.label19.TabIndex = 18;
            this.label19.Text = "preset current:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(147, 39);
            this.label16.TabIndex = 15;
            this.label16.Text = "position:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1480, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(199, 39);
            this.label12.TabIndex = 14;
            this.label12.Text = "AUX4 OUT:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1480, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(199, 39);
            this.label13.TabIndex = 13;
            this.label13.Text = "AUX3 OUT:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1480, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(199, 39);
            this.label14.TabIndex = 12;
            this.label14.Text = "AUX2 OUT:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1480, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(199, 39);
            this.label15.TabIndex = 11;
            this.label15.Text = "AUX1 OUT:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1129, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(160, 39);
            this.label11.TabIndex = 10;
            this.label11.Text = "AUX4 IN:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1129, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 39);
            this.label10.TabIndex = 9;
            this.label10.Text = "AUX3 IN:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1129, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 39);
            this.label9.TabIndex = 8;
            this.label9.Text = "AUX2 IN:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1129, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 39);
            this.label8.TabIndex = 7;
            this.label8.Text = "AUX1 IN:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(810, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 39);
            this.label7.TabIndex = 6;
            this.label7.Text = "Theta:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(810, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "R:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(810, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Y:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(810, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "X:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Red;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(281, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "power supply";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Red;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(729, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "LockIn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "stage";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1864, 829);
            this.Controls.Add(this.gp_Status);
            this.Controls.Add(this.gp_Plot);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "TrMoke - Main Window";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gp_Plot.ResumeLayout(false);
            this.gp_Status.ResumeLayout(false);
            this.gp_Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mainToolStripMenuItem;
        private System.Windows.Forms.GroupBox gp_Plot;
        private System.Windows.Forms.GroupBox gp_Status;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private OxyPlot.WindowsForms.Plot plot1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}

